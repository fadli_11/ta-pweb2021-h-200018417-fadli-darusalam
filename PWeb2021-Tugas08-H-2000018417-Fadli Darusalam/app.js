function data(){
    var nama = document.getElementById("txtNama").value;
    var nim = document.getElementById("txtNim").value;
    var alamat = document.getElementById("txtAlamat").value;

    if (nama != "" && nim != "" && alamat != ""){
        return true;
    }
    else{
        alert("Anda Harus Mengisi Semuanya!!")
    }
}

function pilihan(){
    var prodi = document.getElementById("txtProdi").value;
    var agama = document.getElementById("txtAgama").value;

    if(prodi == '0' || agama == '0'){
        alert("Prodi dan Agama harus dipilih !!")
    }
    else{
        return true;
    }
}

function huruf(nilai){
    var alfabet = /^[a-zA-Z]+$/;

    if(nilai.value == ""){
        return true;
    }
    else if(nilai.value.match(alfabet)){
        return true;
    }
    else{
        alert("Nama hanya terdiri dari Huruf !!")
    }
}

function bilangan(nilai){
    var angka = /^[0-9]+$/;

    if(nilai.value == ""){
        return true;
    }
    else if(nilai.value.match(angka)){
        return true;
    }
    else{
        alert("NIM hanya terdiri dari Angka !!")
    }
}

function check(){
    var klik = document.getElementById("centang").checked;

    if(klik == true){
        return true;
    }
    else{
        alert("Anda harus mencentang pernyatan !!")
    }
}

function validasi(){
    data();
    pilihan();
    bilangan(document.getElementById("txtNim"));
    huruf(document.getElementById("txtNama"));
    check();
}