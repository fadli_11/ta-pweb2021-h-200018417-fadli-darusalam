function validasiSatu(){
    validasi_signup();
    bilangan(document.getElementById("nim"));
    mail(document.getElementById("email"));
    check();
    pass();
}

function validasiDua(){
    validasi_login();
    pass();
}

function validasiTiga(){
    validasi_guestbook();
    mail(document.getElementById("email"));
}

function validasi_guestbook(){
    var name = document.getElementById("nama").value;
    var address = document.getElementById("alamat").value;
    var email = document.getElementById("email").value;
    var comment = document.getElementById("komentar").value;

    if(name != "" && address != "" && email != "" && comment != ""){
        alert("Terima kasih sudah mengisi buku tamu..");
        return true;
    } else{
        alert("Anda harus mengisi semuanya !!");
    }
}

function validasi_login(){
    var uname = document.getElementById("username").value;
    var passwd = document.getElementById("password").value;

    if(uname != "" && passwd != ""){
        return true;
    } else{
        alert("Anda harus mengisi semuanya !!");
    }
}

function validasi_signup(){
    var xusername = document.getElementById("username").value;
    var xemail = document.getElementById("email").value;
    var xnim = document.getElementById("nim").value;
    var xttl = document.getElementById("ttl").value;
    var xpassword = document.getElementById("password").value;
    
    if(xusername != "" && xemail != "" && xnim != "" && xttl != "" && xpassword != ""){
        return true;
    } else {
        alert("Anda harus mengisi semuanya !!");
    }
}

function bilangan(nilai){
    var angka = /^[0-9]+$/;

    if(nilai.value == ""){
        return true;
    } else if(nilai.value.match(angka)){
        return true;
    } else{
        alert("NIM hanya terdiri dari Angka !!");
        return false;
    }
}

function check(){
    var klik = document.getElementById("centang").checked;

    if(klik == true){
        return true;
    }
    else{
        alert("Anda harus mencentang pernyatan !!");
        return false;
    }
}

function pass(){
    var pw = document.getElementById("password").value;

    if(pw.length != 8){
        alert("Password harus 8 karakter !!");
    } else{
        return true;
    }
}

function mail(isi){
    var email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    
    if(isi.value.match(email)){
        return true;
    } else{
        alert("Format Email salah !!");
        return false;
    }
}