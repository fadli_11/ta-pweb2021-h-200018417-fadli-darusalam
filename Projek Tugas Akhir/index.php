<html>
    <head>
        <title>Tugas Akhir</title>
        <link rel="stylesheet" href="style.css">
        <script language="Javascript" src="app.js"></script>
    </head>
    <body>
        <section>
        <header>
            <nav>
                <a href="#" class="title">Tugas Akhir</a>
                <ul>
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">PROFIL</a></li>
                    <li><a href="signup.php">SIGN UP</a></li>
                    <li><a href="login.php">LOGIN</a></li>
                </ul>
            </nav>
        </header>
        <div class="greeting">
			<p>Hai,
				<br>Saya Fadli Darusalam,
				<br>Seorang Mahasiswa dari
				<br>Universitas Ahmad Dahlan
                <br>Selamat Datang di Website saya..
     		 </p>
		</div>
		<img src="./images/uuad.png" class="logo-uad">
        </section>
        <main>
            <div class="profile">
                <img src="./images/foto.jpg" />
                <div class="deskripsi">
                <a name="deskripsi"></a>
                    <p>
                        Assalamu'alaikum... <br>Perkenalkan, saya 
                        Fadi Darusalam. Saya lahir di Sragen tepat pada 
                        malam tahun baru tahun 2003. Saat ini, saya 
                        merupakan seorang Mahasiswa dari Prodi Teknik 
                        Informatika di Universitas Ahmad Dahlan Yogyakarta. 
                        <br><br>Hobi saya adalah membaca buku dan melakukan 
                        berbagai olah raga seperti futsal, badminton, dan 
                        tenis meja. Selain menjadi seorang mahasiswa, saya 
                        juga kadang mengisi waktu luang dengan menjadi 
                        seorang Bug Hunter bersama tim saya. Saya mencari 
                        bug/kelemahan pada sebuah website, kemudian 
                        melaporkannya kepada pemilik website agar segera 
                        diperbaiki dan agar tidak disalahgunakan oleh 
                        orang lain. Terkadang, ada seorang admin website 
                        yang memberikan bounty berupa uang atau sertifikat.
                    </p>
                </div>
            </div>
            <br><br><br><br><br>
		<div class="profile">
			<div class="atr">
			<h2>Buku Tamu</h2>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" name="form1">
                <table>
                    <tr>
                        <td>Nama Lengkap</td>
                        <td>:</td>
                        <td><input type="text" id="nama" name="nama"></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><input type="text" id="alamat" name="alamat"></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>E-Mail</td>
                        <td>:</td>
                        <td><input type="text" id="email" name="email"></td>
                    </tr>
                    <tr></tr>					
					<tr>
						<td>Status</td>
						<td>:</td>
						<td>
							<select name="status" id="status">
								<option value="menikah">Menikah</option>
								<option value="single">Single</option>
							</select>
						</td>
					</tr>
					<tr></tr>
                    <tr>
                        <td>Komentar</td>
                        <td>:</td>
                        <td><textarea name="komentar" id="komentar"></textarea></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
							<input type="submit" name="submit" value="Kirim" onclick="validasiTiga()">
							<input type="reset" name="submit2" value="Batal">
						</td>
                    </tr>
					<tr>
						<td></td>
						<td></td>
						<td><a href="./Guestbook/lihat.php">Lihat Buku Tamu</a></td>
					</tr>
                </table>
            </form>
            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST"){
                $nama = $_POST['nama'];
                $alamat = $_POST['alamat'];
                $email = $_POST['email'];
                $status = $_POST['status'];
                $komentar = $_POST['komentar'];

                $fp = fopen("./Guestbook/guestbook.txt", "a+");
                fputs($fp, "$nama|$alamat|$email|$status|$komentar\n");
                fclose($fp);
            }
            ?>
			</div>
			<img src="./images/asdf.png" width="350px">
		</div><br><br><br><br><br><br><br><br>
        </main>
        <footer>
            <a href="https://gitlab.com/fdl_11"><img src="./images/git.png" width="30px"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://instagram.com/fdl._.11"><img src="./images/ig.png" width="30px"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://wa.me/62895329127575"><img src="./images/wa.png" width="30px"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://www.linkedin.com/in/fadli-darusalam-52a396203"><img src="./images/in.png" width="30px"></a>
        </footer>
    </body>
</html>