<html>
    <head>
        <title>PHP</title>
        <style type="text/css">
            body{
                background: url('../gambar/bgg.jpeg');
                background-repeat: no-repeat;
                background-size: cover;
            }
            .container{
                width: 350px;
                border: 3px solid black;
                height: 300px;
                color: black;
                border-radius: 3px;
                box-sizing: border-box;
                margin: 0 auto;
                place-items: center;
                display: grid;
                position: relative;
                top: 50%;
                margin-top: 150px
            }
            .container-1{
                width: 80%;
                border-bottom: 1px solid black;
                padding-left: 30px;
            }
            .container-2{
                width: 80%;
                padding-left: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="container-1"><br>
                <?php
                    $gaji = 100000;
                    $pajak = 0.1;
                    $thp = $gaji - ($gaji*$pajak);
                    echo "PROGRAM 9.1";
                    echo "<br> Gaji sebelum pajak = Rp.$gaji <br>";
                    echo "Gaji yang dibawa pulang = Rp.$thp <br>";
                ?><br><br>
            </div>
            <div class="container-2">
                <?php
                    $a = 5;
                    $b = 4;

                    echo "PROGRAM 9.2";
                    echo "<br>$a == $b : ". ($a == $b);
                    echo "<br>$a != $b : ". ($a != $b);
                    echo "<br>$a > $b : ". ($a > $b);
                    echo "<br>$a < $b : ". ($a < $b);
                    echo "<br>($a != $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
                    echo "<br>($a != $b) || ($a > $b) : ".(($a != $b) || ($a > $b));
                ?><br><br>
            </div>
        </div>
    </body>
</html>
