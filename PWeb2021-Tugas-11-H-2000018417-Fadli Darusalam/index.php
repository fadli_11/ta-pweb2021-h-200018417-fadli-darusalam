<?php

$xnama = $xemail = $xgender = $xnim = $xalamat = "";
$nama = $email = $gender = $alamat = $nim = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nama"])) {
    $xnama = "Nama harus diisi !!";
  } else {
    $nama = test_input($_POST["nama"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$nama)) {
      $xnama = "Hanya boleh huruf dan spasi !!"; 
      $nama = "";
    }
  }
  
  if (empty($_POST["email"])) {
    $xemail = "Email harus diisi !!";
  } else {
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $xemail = "Format email salah !!"; 
      $email = "";
    }
  }
    
  if (empty($_POST["nim"])) {
    $xnim = "NIM Harus diisi";
  } else {
    $nim = test_input($_POST["nim"]);
    if (!preg_match("/^[0-9]*$/",$nim)) {
      $xnim = "Hanya angka yang diperbolehkan"; 
      $nim = "";
    }
  }

  if (empty($_POST["alamat"])) {
    $xalamat = "Alamat Harus diisi";
  } else {
    $alamat = test_input($_POST["alamat"]);
  }

  if (empty($_POST["gender"])) {
    $xgender = "Gender harus dipilih";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<html>
  <head>
    <title>TUGAS 11</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="container">
      <div class="kiri">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
          <table border="0" cellpadding="5" width="520" align="center">
            <br><h2>FORM BIODATA</h2>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td>
                <input class="form-content" type="text" name="nama" size="20" value="<?php echo $nama; ?>">
                <span class="error">*<?php echo $xnama;?></span>
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td>NIM</td>
              <td>:</td>
              <td>
                <input class="form-content" type="text" name="nim" size="20" value="<?php echo $nim; ?>">
                <span class="error">*<?php echo $xnim;?></span>
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td>Email</td>
              <td>:</td>
              <td>
                <input class="form-content" type="text" name="email" size="25" value="<?php echo $email; ?>">
                <span class="error">*<?php echo $xemail;?></span>
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td>Alamat</td>
              <td>:</td>
              <td>
                <textarea class="form-content" name="alamat" cols="25" rows="2" value="<?php echo $alamat?>"></textarea>
                <span class="error">*<?php echo $xalamat;?></span>
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td>Gender</td>
              <td>:</td>
              <td>
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Laki-laki") echo "checked";?> value="laki-laki">Laki-Laki
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Perempuan") echo "checked";?> value="Perempuan">Perempuan
                <span class="error">* <?php echo $xgender;?></span>
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td></td>
              <td></td>
              <td><input type="submit" value="SUBMIT"></td>
            </tr>
          </table>
        </form>
      </div>
      <div class="kanan">
      <?php
        echo "<h2>Hasil :</h2>";
        echo $nama;
        echo "<br><br>";
        echo $email;
        echo "<br><br>";
        echo $nim;
        echo "<br><br>";
        echo $alamat;
        echo "<br><br>";
        echo $gender;
      ?>
      </div>
    </div>
  </body>
</html>