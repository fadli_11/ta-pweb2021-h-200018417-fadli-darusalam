<?php
    $nilai = $_POST['nilai'];
    
    if($nilai >= 80){
        $hasil = "Predikat Anda A";
    } else if($nilai >= 76.25){
        $hasil = "Predikat Anda A-";
    } else if($nilai >= 68.75){
        $hasil = "Predikat Anda B+";
    } else if($nilai >= 65){
        $hasil = "Predikat Anda B";
    } else if($nilai >= 62.5){
        $hasil = "Predikat Anda B-";
    } else if($nilai >= 57.5){
        $hasil = "Predikat Anda C+";
    } else if($nilai >= 55){
        $hasil = "Predikat Anda C";
    } else if($nilai >= 51.25){
        $hasil = "Predikat Anda C-";
    } else if($nilai >= 43.75){
        $hasil = "Predikat Anda D+";
    } else if($nilai >= 40){
        $hasil = "Predikat Anda D";
    } else{
        $hasil = "Predikat Anda E";
    }
?>
<html>
    <head>
        <title>Program Pengecekan Bilangan Prima</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
    <div class="container"><br>
        <header>
            <h1>KONVERSI ANGKA KE HURUF</h1>
        </header><br>
        <main>
            <div class="kotak">
                <h3>HASIL</h3><br>
                <h3 class="hasil"><?php echo $hasil ?></h3>
            </div>
            <form action="index.php" method="POST">
                <br><br><br><br>
                <input type="submit" value="Cek lagi">
            </form>
        </main>
    </div>
    <footer>
        <p>Copyright &copy; Fadli Darusalam | 2000018417</p>
    </footer>
    </body>
</html>